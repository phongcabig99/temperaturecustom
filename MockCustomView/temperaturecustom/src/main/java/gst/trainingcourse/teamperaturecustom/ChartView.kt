package gst.trainingcourse.teamperaturecustom

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import kotlin.math.min


class ChartView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val dataSet = mutableListOf<DataTemperature>()
    private val path = Path()
    private val borderPath = Path()
    private val trianglePath = Path()
    private val backgroundRect = RectF()
    private var padding = 0F
    private var heightPercent = 0F
    private var xGuidelineEnd = 0F
    private var xTextDay = 0F
    private var yTextDay = 0F
    private var chartWidth = 0F
    private var chartHeight = 0F
    private var radius = 0F
    private val borderColor: Int
    private val startGradientColor: Int
    private val endGradientColor: Int
    private val backgroundMarkerTextColor: Int
    private val gradientColors: IntArray
    private var touching = false
    private var touchPosition = PointF()
    private var index = 0
    private var marginTextMarker = 0F
    private var distanceXOfTriangle = 0F
    private var distanceYOfTriangle = 0F
    private var roundOfBackgroundMarker = 0F

    private val guideLinePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true
        color = Color.GRAY
        style = Paint.Style.FILL
    }

    private val fontMetrics = Paint.FontMetrics()

    private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        textAlign = Paint.Align.CENTER
        color = Color.GRAY
        typeface = Typeface.create("", Typeface.NORMAL)
    }

    private val textMarkerPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = Color.WHITE
        typeface = Typeface.create("", Typeface.NORMAL)
    }

    private val backgroundTextPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true
        style = Paint.Style.FILL
    }

    private val circlePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true
        style = Paint.Style.FILL
    }

    private val pathPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true
        style = Paint.Style.FILL
    }

    private val borderPathPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
    }

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.ChartView)
        borderColor = typedArray.getColor(R.styleable.ChartView_borderPathColor, 0)
        backgroundMarkerTextColor =
            typedArray.getColor(R.styleable.ChartView_backgroundMarkerTextColor, 0)
        startGradientColor = typedArray.getColor(R.styleable.ChartView_startGradientPathColor, 0)
        endGradientColor = typedArray.getColor(R.styleable.ChartView_endGradientPathColor, 0)
        gradientColors = intArrayOf(
            startGradientColor,
            endGradientColor,
        )
        typedArray.recycle()
    }


    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        padding = (min(width, height) / 10).toFloat()
        heightPercent = (height - 2 * padding) / 4
        xGuidelineEnd = width - padding
        xTextDay = width - 9 * padding / 4
        yTextDay = height - padding / 2
        chartWidth = width - 2 * padding
        chartHeight = height - 2 * padding
        pathPaint.shader = LinearGradient(
            padding,
            padding + chartHeight,
            padding,
            padding,
            gradientColors,
            null,
            Shader.TileMode.CLAMP
        )
        borderPathPaint.strokeWidth = (min(width, height) / 90).toFloat()
        textPaint.textSize = (min(width, height) / 26).toFloat()
        radius = borderPathPaint.strokeWidth * 2
        marginTextMarker = radius
        textMarkerPaint.textSize = (min(width, height) / 26).toFloat()
        distanceXOfTriangle = radius
        distanceYOfTriangle = radius * 5 / 2
        roundOfBackgroundMarker = radius
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        drawTextGuidelines(canvas)
        drawDayText(canvas)
        drawPath(canvas)
        drawGuidelines(canvas)
        drawWhenTouching(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {

                val xPosition = event.x
                val yPosition = event.y

                if (xPosition in padding..padding + chartWidth && yPosition in padding..padding + chartWidth) {
                    touching = true
                    val percent = chartWidth / dataSet.size
                    index = ((xPosition - padding) / percent).toInt()

                    touchPosition = realPoint(index, dataSet)
                    invalidate()
                }
            }

            MotionEvent.ACTION_UP -> {
                touching = false
                performClick()
            }

            MotionEvent.ACTION_CANCEL -> {
                touching = false
            }
        }

        return true
    }

    // Because we call this from onTouchEvent, this code will be executed for both
    // normal touch events and for when the system calls this using Accessibility
    override fun performClick(): Boolean {
        super.performClick()
        launchMissile()
        return true
    }

    private fun launchMissile() {
        Log.d("ChartView", "#launchMissile(): Missile launched")
    }

    fun setData(newDataSet: List<DataTemperature>) {
        dataSet.addAll(newDataSet)
        invalidate()
    }

    private fun realPoint(index: Int, temperatures: List<DataTemperature>): PointF {
        val realX = padding + chartWidth * index / (temperatures.size - 1)
        val realY = padding + (10 - temperatures[index].temperatureVal) * chartHeight / 40
        return PointF(realX, realY)
    }

    private fun conPoint1(point1: PointF, point2: PointF): PointF {
        val x = (point1.x + point2.x) / 2
        val y = point1.y
        return PointF(x, y)
    }

    private fun conPoint2(point1: PointF, point2: PointF): PointF {
        val x = (point1.x + point2.x) / 2
        val y = point2.y
        return PointF(x, y)
    }

    private fun drawPath(canvas: Canvas) {
        path.reset()

        val startRealPoint = realPoint(0, dataSet)
        path.moveTo(startRealPoint.x, startRealPoint.y)

        for (i in 1 until dataSet.size) {
            val realPoint = realPoint(i - 1, dataSet)
            val nextRealPoint = realPoint(i, dataSet)
            val conPoint1 = conPoint1(realPoint, nextRealPoint)
            val conPoint2 = conPoint2(realPoint, nextRealPoint)

            path.cubicTo(
                conPoint1.x,
                conPoint1.y,
                conPoint2.x,
                conPoint2.y,
                nextRealPoint.x,
                nextRealPoint.y
            )
        }

        borderPath.set(path)
        path.lineTo(padding + chartWidth, padding + chartHeight)
        path.lineTo(padding, padding + chartHeight)
        path.lineTo(padding, padding)

        canvas.drawPath(path, pathPaint)
        borderPathPaint.color = borderColor
        canvas.drawPath(borderPath, borderPathPaint)
    }

    private fun drawGuidelines(canvas: Canvas) {
        for (i in 0..4) {
            canvas.drawLine(
                padding,
                padding + i * heightPercent,
                xGuidelineEnd,
                padding + i * heightPercent,
                guideLinePaint
            )
        }
    }

    private fun drawTextGuidelines(canvas: Canvas) {
        for (i in 0..4) {
            canvas.drawText(
                (-10 * i + 10).toString(),
                padding / 2,
                padding + i * heightPercent,
                textPaint
            )
        }
    }

    private fun drawDayText(canvas: Canvas) {
        canvas.drawText(dataSet[0].dayVal, padding * 2, yTextDay, textPaint)
        canvas.drawText(
            dataSet[dataSet.size - 1].dayVal,
            xTextDay + padding / 4,
            yTextDay,
            textPaint
        )
    }

    private fun drawWhenTouching(canvas: Canvas) {
        if (touching) {
            canvas.drawLine(
                touchPosition.x,
                touchPosition.y,
                touchPosition.x,
                padding + chartHeight,
                borderPathPaint
            )
            circlePaint.color = borderColor
            canvas.drawCircle(touchPosition.x, touchPosition.y, radius, circlePaint)

            val textMarker = "${dataSet[index].dayVal} | ${dataSet[index].temperatureVal} °C"
            textMarkerPaint.getFontMetrics(fontMetrics)

            backgroundTextPaint.color = backgroundMarkerTextColor

            val xMarker = when (index) {
                0 -> touchPosition.x - padding / 2
                in 1 until dataSet.size - 2 -> touchPosition.x - padding
                else -> touchPosition.x - padding * 5 / 2
            }

            val yMarker = touchPosition.y + padding

            trianglePath.reset()
            trianglePath.moveTo(touchPosition.x, touchPosition.y + radius / 3)
            trianglePath.lineTo(
                touchPosition.x - distanceXOfTriangle,
                touchPosition.y + distanceYOfTriangle
            )
            trianglePath.lineTo(
                touchPosition.x + distanceXOfTriangle,
                touchPosition.y + distanceYOfTriangle
            )

            canvas.drawPath(trianglePath, backgroundTextPaint)

            backgroundRect.set(
                xMarker - marginTextMarker,
                yMarker + fontMetrics.top - marginTextMarker,
                xMarker + textMarkerPaint.measureText(textMarker) + marginTextMarker,
                yMarker + fontMetrics.bottom + marginTextMarker
            )

            canvas.drawRoundRect(
                backgroundRect,
                roundOfBackgroundMarker,
                roundOfBackgroundMarker,
                backgroundTextPaint
            )

            canvas.drawText(
                textMarker,
                xMarker,
                yMarker,
                textMarkerPaint
            )
        }
    }
}

data class DataTemperature(
    val dayVal: String,
    val temperatureVal: Int
)