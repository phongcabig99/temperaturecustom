package gst.trainingcourse.teamperaturecustom

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.graphics.Typeface
import android.graphics.Shader
import android.graphics.LinearGradient
import android.animation.ValueAnimator.AnimatorUpdateListener
import android.util.Log
import android.view.MotionEvent
import kotlin.math.*


class TemperatureView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr), AnimatorUpdateListener {

    private val labelArray = listOf(
        resources.getString(R.string.negative_20),
        resources.getString(R.string.negative_10),
        resources.getString(R.string.zero),
        resources.getString(R.string.positive_10),
        resources.getString(R.string.positive_20),
    )

    private val numbersLabel = IntArray(41) { it - 20 }

    private val paintCircle = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        // Paint styles used for rendering are initialized here. This
        // is a performance optimization, since onDraw() is called
        // for every screen refresh.
        isAntiAlias = true //Anti-aliased
        style = Paint.Style.FILL
    }

    private val paintArc = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true //Anti-aliased
        strokeCap = Paint.Cap.ROUND
        style = Paint.Style.STROKE
        color = Color.LTGRAY
    }

    private val paintArcAnim = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true //Anti-aliased
        strokeCap = Paint.Cap.ROUND
        style = Paint.Style.STROKE
    }

    private val paintLine = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true //Anti-aliased
        color = Color.LTGRAY
        style = Paint.Style.STROKE //Set to Hollow
        strokeWidth = 4F //Set width
    }

    private val paintLineAnim = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true //Anti-aliased
        color = Color.GREEN
        style = Paint.Style.STROKE //Set to Hollow
        strokeWidth = 4F //Set width
    }

    private val paintTextLabel = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        textAlign = Paint.Align.CENTER
        typeface = Typeface.create("", Typeface.NORMAL)
    }

    private val paintText = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        textAlign = Paint.Align.CENTER
        color = Color.WHITE
        typeface = Typeface.create("", Typeface.NORMAL)
    }

    private var radiusTextLabel = 0.0F
    private var radiusGauge = 0.0F
    private var radiusArc = 0.0F
    private var radiusCircle = 0.0F // Radius of the circle.
    private var radiusOffsetGauge1 = 0.0F
    private var radiusOffsetGauge2 = 0.0F
    private var sweepAngleArc = 0.0F
    private var textCenter = labelArray[0]
    private var valueOfSweepArc = 0
    private val oval = RectF()

    //Point at which to draw label and indicator circle position. PointF is a point
    //with floating-point coordinates.
    private val pointPosition: PointF = PointF(0.0F, 0.0F)
    private val startGradientColor: Int
    private val mediumGradientColor1: Int
    private val mediumGradientColor2: Int
    private val mediumGradientColor3: Int
    private val endGradientColor: Int
    private val gradientColors: IntArray

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.TemperatureView)
        startGradientColor = typedArray.getColor(R.styleable.TemperatureView_startGradientColor, 0)
        mediumGradientColor1 =
            typedArray.getColor(R.styleable.TemperatureView_mediumGradientColor1, 0)
        mediumGradientColor2 =
            typedArray.getColor(R.styleable.TemperatureView_mediumGradientColor2, 0)
        mediumGradientColor3 =
            typedArray.getColor(R.styleable.TemperatureView_mediumGradientColor3, 0)
        endGradientColor = typedArray.getColor(R.styleable.TemperatureView_endGradientColor, 0)
        gradientColors = intArrayOf(
            startGradientColor,
            mediumGradientColor1,
            mediumGradientColor2,
            mediumGradientColor3,
            endGradientColor,
        )
        typedArray.recycle()
    }

    companion object {
        private const val START_ANGLE_ARC = 150F
        private const val SWEEP_ANGLE_ARC = 240F
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        // Calculate the radiusTextLabel from the smaller of the width and height.
        radiusTextLabel = (min(width, height) / 2 * 0.9).toFloat()
        radiusGauge = radiusTextLabel - (radiusTextLabel / 4F)
        radiusArc = radiusGauge - (radiusTextLabel / 10F)
        radiusCircle = radiusArc - (radiusTextLabel / 8F)

        // Gradient for Circle
        paintCircle.shader = LinearGradient(
            (width / 2).toFloat(),
            height / 2 - radiusCircle,
            (width / 2).toFloat(),
            height / 2 + radiusCircle,
            gradientColors,
            null,
            Shader.TileMode.CLAMP
        )
        // Gradient and strokeWidth for Arc
        paintArcAnim.apply {
            shader = LinearGradient(
                0F,
                height.toFloat(),
                width.toFloat(),
                0F,
                gradientColors,
                null,
                Shader.TileMode.CLAMP
            )
            strokeWidth = radiusTextLabel / 10F
        }

        paintArc.strokeWidth = radiusTextLabel / 10F
        // Text size for text center
        paintText.textSize = radiusCircle / 3F
        // Text size for text label
        paintTextLabel.textSize = radiusTextLabel / 12F
        // Radius offset gauge to end of gauge line normal
        radiusOffsetGauge1 = radiusTextLabel / 12F
        // Radius offset gauge to end of gauge line higher line normal
        radiusOffsetGauge2 = radiusTextLabel / 8F
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        // Draw the circle.
        canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), radiusCircle, paintCircle)

        // Draw Arc
        val left = width / 2 - radiusArc
        val top = height / 2 - radiusArc
        val right = width - left
        val bottom = height - top
        oval.set(left, top, right, bottom)
        canvas.drawArc(
            oval,
            START_ANGLE_ARC,
            SWEEP_ANGLE_ARC,
            false,
            paintArc
        )

        // Draw Arc Anim
        canvas.drawArc(
            oval,
            START_ANGLE_ARC,
            sweepAngleArc,
            false,
            paintArcAnim
        )

        // Draw gauge and text label
        drawArc(20, paintLine, canvas)

        // Draw gauge animation
        drawArc(valueOfSweepArc, paintLineAnim, canvas)

        // Draw text center
        canvas.drawText(textCenter, (width / 2).toFloat(), (height / 2).toFloat(), paintText)
    }

    private fun drawArc(valueOfSweep: Int, paintLine: Paint, canvas: Canvas) {
        for (i in 0..valueOfSweep) {
            pointPosition.computeXYForGauge(i, radiusGauge)
            val xStart = pointPosition.x
            val yStart = pointPosition.y
            pointPosition.computeXYForGauge(i, radiusGauge + radiusOffsetGauge1)
            val xEnd = pointPosition.x
            val yEnd = pointPosition.y
            pointPosition.computeXYForGauge(i, radiusGauge + radiusOffsetGauge2)
            val xEnd2 = pointPosition.x
            val yEnd2 = pointPosition.y
            pointPosition.computeXYForGauge(i, radiusTextLabel)
            val xTextLabel = pointPosition.x
            val yTextLabel = pointPosition.y


            if (i % 5 == 0) {
                // Draw gauge line higher
                canvas.drawLine(
                    xStart,
                    yStart, xEnd2,
                    yEnd2, paintLine
                )
                // Draw text label
                canvas.drawText(labelArray[i / 5], xTextLabel, yTextLabel, paintTextLabel)
            } else {
                // Draw gauge line normal
                canvas.drawLine(
                    xStart,
                    yStart, xEnd,
                    yEnd, paintLine
                )
            }
        }
    }

    private fun PointF.computeXYForGauge(pos: Int, radius: Float) {
        // Angles are in radians.
        val startAngle = 5 * PI / 6.0 // 150°
        val sweep = 4 * PI / 3.0 // 240°
        val angle = startAngle + pos * (sweep / 20)
        x = (radius * cos(angle)).toFloat() + width / 2
        y = (radius * sin(angle)).toFloat() + height / 2
    }

    fun startAnim() {
        val animator: ValueAnimator = ValueAnimator.ofInt(0, 20)
        // sets the duration of our animation
        animator.duration = 1000
        // registers our AnimatorUpdateListener
        animator.addUpdateListener(this)
        animator.start()
    }

    override fun onAnimationUpdate(animation: ValueAnimator?) {
        //gets the current value of our animation
        val value = animation?.animatedValue as? Int

        if (value != null) {
            sweepAngleArc = (value * SWEEP_ANGLE_ARC) / 20

            valueOfSweepArc = getValueOfSweep()

            if (value % 5 == 0) {
                textCenter = labelArray[value / 5]
            }
        }

        // Forces the view to reDraw itself
        invalidate()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_MOVE -> {
                if (distance(
                        event.x,
                        event.y,
                        width / 2F,
                        height / 2F
                    ) >= radiusArc - 50F && distance(
                        event.x,
                        event.y,
                        width / 2F,
                        height / 2F
                    ) <= radiusArc + 50F && event.y <= height / 2 + radiusArc / 2
                ) {

                    val xPosition = event.x
                    val yPosition = event.y

                    if (oval.contains(xPosition, yPosition)) {
                        sweepAngleArc = getSweepAngle(xPosition, yPosition, radiusArc)

                        valueOfSweepArc = getValueOfSweep()
                        val value = 2 * valueOfSweepArc

                        if (value in 0..40) {
                            textCenter = numbersLabel[value].toString() + "°C"
                        }

                        invalidate()
                    }
                }
            }

            MotionEvent.ACTION_UP -> {
                performClick()
            }
        }

        return true
    }

    // Because we call this from onTouchEvent, this code will be executed for both
    // normal touch events and for when the system calls this using Accessibility
    override fun performClick(): Boolean {
        super.performClick()
        launchMissile()
        return true
    }

    private fun launchMissile() {
        Log.d("TemperatureView", "#launchMissile(): Missile launched")
    }

    private fun distance(x1: Float, y1: Float, x2: Float, y2: Float): Float {
        return sqrt(((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)))
    }

    private fun getSweepAngle(xTouch: Float, yTouch: Float, radius: Float): Float {
        val startAngle = 5 * PI / 6.0 // 150°

        val angle = if (yTouch > height / 2) {
            if (xTouch < width / 2) {
                acos((xTouch - width / 2) / radius)
            } else {
                (2 * PI + acos((xTouch - width / 2) / radius)).toFloat()
            }
        } else {
            (2 * PI - acos((xTouch - width / 2) / radius)).toFloat()
        }

        var sweepAngle = ((angle - startAngle) * 180 / PI).toFloat()
        if (sweepAngle < 0F) {
            sweepAngle = 0F
        }
        if (sweepAngle > 240F) {
            sweepAngle = 240F
        }
        return sweepAngle
    }

    private fun getValueOfSweep(): Int {
        val percent = round(sweepAngleArc * 41 / SWEEP_ANGLE_ARC).toInt()
        return percent / 2
    }

}