package gst.trainingcourse.mockcustomview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * This is base Activity class
 */
abstract class BaseActivity : AppCompatActivity() {

    abstract fun getLayoutID(): Int
    abstract fun onCreateActivity(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutID())
        mapping()
        onCreateActivity(savedInstanceState)
    }

    open fun mapping() = Unit

}