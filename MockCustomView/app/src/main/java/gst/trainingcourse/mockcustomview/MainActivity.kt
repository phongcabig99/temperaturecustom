package gst.trainingcourse.mockcustomview


import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import gst.trainingcourse.teamperaturecustom.ChartView
import gst.trainingcourse.teamperaturecustom.DataTemperature
import gst.trainingcourse.teamperaturecustom.TemperatureView

class MainActivity : BaseActivity() {

    private var temperatureView: TemperatureView? = null
    private var buttonStart: AppCompatButton? = null
    private var chartView: ChartView? = null

    override fun getLayoutID(): Int {
        return R.layout.activity_main
    }

    override fun onCreateActivity(savedInstanceState: Bundle?) {
        val data = DataTemperature("26/05/2021", 10)
        val data1 = DataTemperature("27/05/2021", -3)
        val data2 = DataTemperature("28/05/2021", 9)
        val data3 = DataTemperature("29/05/2021", -15)
        val data4 = DataTemperature("30/05/2021", -8)
        val data5 = DataTemperature("31/05/2021", -10)
        val data6 = DataTemperature("01/06/2021", -2)
        val data7 = DataTemperature("02/06/2021", -12)
        val data8 = DataTemperature("03/06/2021", -7)
        val data9 = DataTemperature("04/06/2021", -12)
        val data10 = DataTemperature("05/06/2021", 8)

        val newDataPoint = listOf(data, data1, data2, data3, data4, data5, data6, data7, data8, data9, data10)

        chartView?.setData(newDataPoint)

        buttonStart?.setOnClickListener {
            temperatureView?.startAnim()
        }
    }

    override fun mapping() {
        super.mapping()
        temperatureView = findViewById(R.id.temperatureView)
        buttonStart = findViewById(R.id.buttonStart)
        chartView = findViewById(R.id.chartView)
    }

}